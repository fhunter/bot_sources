package require sqlite3
set seen_flood 5:30
set seen_chanreq 1
set seen_otherbots "^CountBlah"
set seen_recommend_private 0
set seen_botnet_note 1
set seen_privmsg 1

sqlite3 seen_db_handle seen.db
seen_db_handle eval { create table if not exists 'seen' (id integer primary key asc autoincrement, unixtime integer not null, channel text not null, nickname text not null, username text not null, ip text not null, joined bool not null, quitmessage text default "" not null); }

# Returns nick in correct case, as in userfile
proc nickcase { nick } {
	set nicklwr [string tolower $nick]
	return "$nicklwr"
}

# returns list "unixtime channel"
proc seen_laston { nick } {
	global seen_db_handle
	set nick [string tolower $nick]
	set unixtime 0
	set channel ""
	set quitmessage ""
	seen_db_handle eval { select * from (select unixtime,channel,quitmessage from seen where nickname=$nick order by unixtime desc, channel) group by channel } values {
		if { $values(unixtime) > $unixtime } {
			set unixtime $values(unixtime)
			set channel $values(channel)
			set quitmessage $values(quitmessage)
		}
	}
	if { $unixtime == 0 } {
		return [list]
	} else {
		return [list $unixtime $channel $quitmessage]
	}
}

proc seen_setlaston { nick uhost channel reason} {
	global seen_db_handle
	set nick [string tolower $nick]
	set lst [split "$uhost" "@"]
	set uname [lindex "$lst" 0]
	set uip [lindex "$lst" 1]
	set timetime [unixtime]
	seen_db_handle eval { insert into 'seen' (unixtime, channel, nickname, username, ip, joined, quitmessage) values ($timetime, $channel, $nick, $uname, $uip, 0, $reason); }
}

# -----------------------------------------------------------------------------

# Bindings
bind pub - !seen pub_seen
bind msg - seen msg_seen
bind msg - !seen msg_seen

# Save "Quit msg" on quit, split, part or kick
bind sign - * sign:seen
proc sign:seen {nick uhost handle channel reason} {
	set quitmessage "quit: $reason"
	seen_setlaston $nick $uhost $channel $quitmessage
}

bind splt - * splt:seen
proc splt:seen {nick uhost handle channel} {
	seen_setlaston $nick $uhost $channel "split"
}
bind part - * part:seen
proc part:seen {nick uhost handle channel {reason ""}} {
	seen_setlaston $nick $uhost $channel "part"
}
bind kick - * kick:seen
proc kick:seen {nick uhost handle channel kicked reason} {
	seen_setlaston $kicked "unknown@unknown.ip" $channel "kicked: $nick - $reason"
}
bind nick - * nick:seen
proc nick:seen {nick uhost handle channel newnick} {
	seen_setlaston $nick $uhost $channel "nick change to: $newnick"
}

# The main seen routine
proc do_seen { user uhost seennick seenchan } {
	global botnick
	putlog "do_seen $seennick $seenchan"

# Initialize things
	if { $seenchan == "" } { set inchan [lindex [channels] 0] } { set inchan $seenchan }
	set randnick [lindex [chanlist $inchan] [rand [llength [chanlist $inchan]]]]
#
#	# Find anyone with that nick (either normal or altnick)
	#set validnick [nickcase [findnick $seennick]]
	set validnick $seennick	
#	if {[string tolower $validnick] != [string tolower $seennick]} {
#		set realuser "$seennick (aka $validnick)"
#	} {
	set realuser "$validnick"
#	}
#
## Some stupid situations
	if {[string compare [string tolower $botnick] [string tolower $seennick]] == 0 || \
	    [string compare [string tolower $botnick] [string tolower $validnick]] == 0} {
		return "yeah, whenever I look in a mirror."
	}
	set seen_self {
		"try looking in a mirror."
		"yes, I see you."
		"if I were a normal bot, I would say 'Trying to find yourself, eh?'"
		"hmm, I think you are %nick, or am I wrong?"
		"strange, you look just like %nick!"
		"I think %randnick knows something about you..."
	}
	set self 0
	if {[string tolower $seennick] == [string tolower $user]} { set self 1 }
	if {$self} {
		set chosen [lindex $seen_self [rand [llength $seen_self]]]
		regsub -all "%randnick" $chosen "$randnick" chosen
		regsub -all "%nick" $chosen "$seennick" chosen
		return "$chosen"
	}
	set output ""

# Nick was here, but got net-split
	set splitchan ""
	foreach chan [channels] {
		if {[onchansplit $seennick $chan]} {
			lappend splitchan $chan
		}
	}
	if {$splitchan != ""} {
		regsub -all " " $splitchan ", " splitchan
		return "$seennick was on a moment ago, but got netsplitted. :("
	}

# Nick is online somewhere
	set onchan ""
	foreach chan [channels] {
		if {$chan == "#ops" } {	continue }
		if {[onchan $seennick $chan]} { lappend onchan $chan }
	}
	if {$onchan != ""} {
		regsub -all " " $onchan ", " onchan
		return "$seennick is in $onchan right now!"
	}

## Still not found, search channels lists, maybe he is using a different nick
	set splitchan ""
	set onchan ""
	# List searched, was he found?
	if {[llength $splitchan] > 0} {
		regsub -all " " $splitchan ", " splitchan
		return "$realuser was on $splitchan a moment ago, but got netsplitted. :("
	}
	if {[llength $onchan] > 0} {
		if {[string compare [string tolower $realnick] [string tolower $user]] == 0} {
			set seen_self {
				"correct me if I am wrong, but I think you are %nick!"
				"you are %nick!"
				"I think you are %nick, or am I wrong?"
				"yes, I see you, %nick!"
				"%nick and %curnick are the same... And that is *you*!"
			}
			set chosen [lindex $seen_self [rand [llength $seen_self]]]
			regsub -all "%randnick" $chosen "$randnick" chosen
			regsub -all "%curnick" $chosen "$realnick" chosen
			regsub -all "%nick" $chosen "$seennick" chosen
			return "$chosen"
		} {
			if {[string compare [string tolower $realnick] [string tolower $seennick]] == 0} {
				set output "$realuser"
			} {
				set output "$realuser is $realnick, and $realnick"
			}
			regsub -all " " $onchan ", " onchan
			return "$output is in $onchan right now!"
		}
	}

# Well, he is definitively NOT online. So go to userlist now.
	set laston [seen_laston $validnick]
	putlog "$laston"
	if {[llength $laston] == 0 } {
		# not even in the userlist, thats bad... :(
		set seen_unknown {
			"I think I don't know %nick yet."
			"hmmm... %nick, this name is not strange to me, but I don't know, sorry."
			"you must introduce me to %nick one day, must be a great person!"
			"I don't know anything about %nick, don't you mean %randnick?"
			"I don't know %nick, ask %randnick, maybe he knows something."
		}
		set chosen [lindex $seen_unknown [rand [llength $seen_unknown]]]
		regsub -all "%randnick" $chosen "$randnick" chosen
		regsub -all "%nick" $chosen "$seennick" chosen
		return "$chosen"
	}

	# Yes, we got him!
	set lasttime [lindex $laston 0]
	set lastchan [lindex $laston 1]
	if {$lastchan == "#ops" } {
		set lastchan "secret channel"
	}
	set reason [lindex $laston 2]
	if {$lasttime == 0} {
		return "I know who $realuser is, but never saw him around."
	}
	# How did he left
	# Where was he seen
	if {$lastchan == $seenchan} {
		set lastchan "this channel"
	} {
		set lastchan "$lastchan"
	}

	set totalyear [expr [unixtime] - $lasttime]
	if {$totalyear < 60} {
		if {$lastchan != ""} {
			return "$realuser has left $lastchan$reason less than a minute ago!"
		} {
			return "$realuser has left$reason less than a minute ago!"
		}
	}
	if {$totalyear >= 31536000} {
		set yearsfull [expr $totalyear/31536000]
		set years [expr int($yearsfull)]
		set yearssub [expr 31536000*$years]
		set totalday [expr $totalyear - $yearssub]
	}
	if {$totalyear < 31536000} {
		set totalday $totalyear
		set years 0
	}
	if {$totalday >= 86400} {
		set daysfull [expr $totalday/86400]
		set days [expr int($daysfull)]
		set dayssub [expr 86400*$days]
		set totalhour [expr $totalday - $dayssub]
	}
	if {$totalday < 86400} {
		set totalhour $totalday
		set days 0
	}
	if {$totalhour >= 3600} {
		set hoursfull [expr $totalhour/3600]
		set hours [expr int($hoursfull)]
		set hourssub [expr 3600*$hours]
		set totalmin [expr $totalhour - $hourssub]
	}
	if {$totalhour < 3600} {
		set totalmin $totalhour
		set hours 0
	}
	if {$totalmin >= 60} {
		set minsfull [expr $totalmin/60]
		set mins [expr int($minsfull)]
	}
	if {$totalmin < 60} {
		set mins 0
	}
	if {$years < 1} {set yearstext ""} elseif {$years == 1} {set yearstext "$years year, "} {set yearstext "$years years, "}
	if {$days < 1} {set daystext ""} elseif {$days == 1} {set daystext "$days day, "} {set daystext "$days days, "}
	if {$hours < 1} {set hourstext ""} elseif {$hours == 1} {set hourstext "$hours hour, "} {set hourstext "$hours hours, "}
	if {$mins < 1} {set minstext ""} elseif {$mins == 1} {set minstext "$mins minute"} {set minstext "$mins minutes"}
	set output $yearstext$daystext$hourstext$minstext
	set output [string trimright $output ", "]
	if {$lastchan != ""} {
		return "I last saw $realuser in $lastchan$reason $output ago"
	} {
		return "I last saw $realuser$reason $output ago"
	}
}

# -----------------------------------------------------------------------------

# Avoids pub/msg flooding
proc seen_detectflood { } {
	global seen_flood
	global seen_floodtrigger
	set thr [lindex [split $seen_flood ":"] 0]
	set lapse [lindex [split $seen_flood ":"] 1]
	if {$thr == "" || $thr == 0} { return 0 }
	if {![info exist seen_floodtrigger]} {
		# First time called
		set seen_floodtrigger [list [unixtime] 1]
		return 0
	}
	if {[expr [lindex $seen_floodtrigger 0] + $lapse] <= [unixtime]} {
		# Trigger time has passed, reset counter
		set seen_floodtrigger [list [unixtime] 1]
		return 0
	}
	set lasttime [lindex $seen_floodtrigger 0]
	set times [lindex $seen_floodtrigger 1]
	if {$times >= $thr} {
		# Flood!
		return 1
	}
	set seen_floodtrigger [list $lasttime [expr $times + 1]]
	return 0
}

# -----------------------------------------------------------------------------

proc pub_seen {nick uhost hand chan arg} {
	global seen_privmsg seen_chanreq seen_recommend_private seen_otherbots
	set arg [string trim $arg "\?! "]
	if {[seen_detectflood]} {
		putcmdlog "<$nick@$chan> !$hand! seen $arg (seen flood... not answering!)"
		return ""
	}
	set responded 0
	if {$seen_chanreq == 1} {
		# Always respond privately
		set responded 1
		if {$seen_privmsg} {
			puthelp "PRIVMSG $nick :[do_seen $nick $uhost $arg $chan]"
		} {
			puthelp "NOTICE $nick :[do_seen $nick $uhost $arg $chan]"
		}
		putcmdlog "<$nick@$chan> !$hand! seen $arg"
	} elseif {$seen_chanreq == 2} {
		# Always respond to channel
		set responded 1
		if {$seen_privmsg} {
			puthelp "PRIVMSG $chan :$nick, [do_seen $nick $uhost $arg $chan]"
		} {
			puthelp "NOTICE $chan :$nick, [do_seen $nick $uhost $arg $chan]"
		}
		putcmdlog "<$nick@$chan> !$hand! seen $arg"
	}
}

proc msg_seen {nick uhost hand arg} {
	global seen_privmsg
	set arg [string trim $arg "\?! "]
	if {[seen_detectflood]} {
		putcmdlog "($nick!$uhost) !$hand! seen $arg (seen flood... not answering!)"
		return ""
	}
	if {$seen_privmsg} {
		puthelp "PRIVMSG $nick :[do_seen $nick $uhost $arg ""]"
	} {
		puthelp "NOTICE $nick :[do_seen $nick $uhost $arg ""]"
	}
	putcmdlog "($nick!$uhost) !$hand! seen $arg"
}

putlog "- seen 5.5.4 by Ernst loaded"
