#load util
source /usr/share/eggdrop/scripts/alltools.tcl

#  Read rules data file
set fp [open "scripts/data/rules.txt" r]
set rules_data [read $fp]
close $fp

# Read CountBlah replies
set fp [open "scripts/data/blah.txt" r]
set blah_data [split [read $fp] "\n"]
close $fp

# Read file with greetings
set channels_old_nickname [dict create "none" "someone I can't even remember"]
set fp [open "scripts/data/greeting.txt" r]
set greetings_data [split [read $fp] "\n"]
close $fp

#  Read operator passwords/usernames
# This is a list of operators that have authorised status
# key is HOST, value is nickname
array set ops_authorised {}
# key is nickname, value is password
array set ops_passwords {}
set fp [open "passwords/operpassword.txt" r]
set oper_passwords_data [split [read $fp] "\n"]
close $fp
foreach {read_header read_nick read_level read_pass} $oper_passwords_data {
  putlog "reading passwords header=$read_header nick=$read_nick level=$read_level pass=$read_pass"
  if {$read_header!=""} {
    set real_nick [lindex [split $read_nick "="] 1]
    set real_pass [lindex [split $read_pass "="] 1]
    set ops_passwords($real_nick) $real_pass
  }
}
unset oper_passwords_data

proc is_authorised {host nick} {
  global ops_passwords
  global ops_authorised
  if {[info exists ops_passwords($nick)]} {
    if {[info exists ops_authorised($host)]} {
      if { $ops_authorised($host) == "$nick" } {
        return 1
      }
    }
  }
  return 0
}

bind msg - .help msg:help
bind pub - !help pubm:help
bind join - * join_hello
bind join - "* eowulf!*@*" gag_eowulf
bind pubm - "% *blah*" countblah_reference
#bind pubm - * censorship_function
bind act - * countblah_actreference
bind pub - !rules pubm:rules
bind pub - !sides pubm:sides
bind msg - .rules msg:rules
bind msg - .fnick msg:fnick
bind pub - !meop pubm:meop
bind msg - .login msg:login
bind msg - .bag msg:bag
bind msg - .gag msg:gag
bind msg - .ban msg:ban
bind msg - .banlist msg:banlist
#Still unimplemented commands (for masters)
bind pub - !nav pubm:nav
bind pub - !noping pubm:noping
bind pub - !antitalk pubm:antitalk
bind pub - !at pubm:at
bind pub - !definition pubm:definition
bind pub - trivgame pubm:trivgame
bind pub - ps pubm:ps
bind pub - birthday pubm:birthday
bind pub - sn pubm:sn
bind pub - hl pubm:hl
bind pub - hp pubm:hp
bind pub - fortune pubm:fortune
bind pub - bottle pubm:bottle
bind pub - modelinks pubm:modelinks
bind pub - mboard pubm:mboard
bind pub - jop pubm:jop
bind pub - jvoice pubm:jvoice
bind pub - gbook pubm:gbook
bind pub - joinmode pubm:joinmode
bind pub - joinset pubm:joinset
bind pub - joindefault pubm:joindefault

bind msg - .say msg:say
bind msg - .act msg:act

proc countblah_reference {nick host handle chan text} {
  global blah_data
  putlog "CountBlah reference messages nick=$nick host=$host handle=$handle chan=$chan text=$text"
  set length [llength [lindex $blah_data]]
  set line_number [expr {round(rand()* $length) }]
  set to_put_line [lindex $blah_data $line_number]
  putmsg $chan "$to_put_line"
}

proc gag_eowulf {nick host handle chan} {
  putlog "Gagged nick=$nick host=$host handle=$handle chan=$chan"
  set text_message "No cursing"
  putserv "SHUN +$nick 5h :$text_message"
}

proc censorship_function {nick host handle chan text} {
  set lowernick [string tolower $nick]
  set lowertext [string tolower $text]
  if { [string match "eowulf" "$lowernick"] } {
    set flag 0
    if { [string match "*schlampe*" "$lowertext"] } {
      set flag 1	  
    }
    if { [string match "*fucking*" "$lowertext"] } {
      set flag 1	  
    }
    if { [string match "*cunt*" "$lowertext"] } {
      set flag 1	  
    }
    if { [string match "*tchermans*" "$lowertext"] } {
      set flag 1	  
    }
    if { [string match "*twat*" "$lowertext"] } {
      set flag 1	  
    }
    if { $flag } {
      putlog "Censorship called nick=$nick host=$host handle=$handle chan=$chan text=$lowertext"
      set text_message "No cursing"
      putserv "SHUN +$nick 30m :$text_message"
    }
  }
}

proc countblah_actreference {handle chan action} {
  putlog "CountBlah actreference messages handle=$handle chan=$chan text=$action"
#  set length [llength [lindex $blah_data]]
#  set line_number [expr {round(rand()* $length) }]
#  set to_put_line [lindex $blah_data $line_number]
#  putmsg $chan "$to_put_line"
}

proc msg:login {nick host handle text} {
  global ops_authorised
  global ops_passwords
  putlog ".login nick=$nick host=$host handle=$handle text=$text"
  if {[info exists ops_passwords($nick)]} {
    if {$ops_passwords($nick) == "$text"} {
      set ops_authorised($host) $nick
      putlog "Authorised $nick on $host as known host to bot"
    } else {
      putlog "Wrong password for $nick = $text"
    }
  } else {
    putlog "Nick not known in login $nick"
  }
}

proc msg:say {nick host handle text} {
    if [expr [isop $nick] || [ishalfop $nick]] {
        set channel [lindex [split $text " "] 0]
        set text_message [join [lrange [split $text " "] 1 end] " "]
        putmsg $channel $text_message
    }
}

proc msg:act {nick host handle text} {
    if [expr [isop $nick] || [ishalfop $nick]] {
        set channel [lindex [split $text " "] 0]
        set text_message [join [lrange [split $text " "] 1 end] " "]
        putact $channel $text_message
    }
}

proc msg:help {nick host handle text} {
    putlog "help called from $nick $host $handle $text"
    if [expr [isop $nick] || [ishalfop $nick]] {
	dumpfile $nick "help_op.txt"
    }
    dumpfile $nick "help.txt"
}

proc pubm:help {nick host handle chan text} {
    putlog "help called from $chan : $nick $host $handle $text"
    if [expr [isop $nick] || [ishalfop $nick]] {
	dumpfile $nick "help_op.txt"
    }
    dumpfile $nick "help.txt"
}
proc msg:bag {nick host handle text} {
    putlog "bag called from $nick $host $handle $text"
    if [expr [isop $nick] || [ishalfop $nick]] {
	putlog "op confirmed for $nick"
#        set channel [lindex [split $text " "] 0]
#        set text_message [join [lrange [split $text " "] 1 end] " "]
#        putmsg $channel $text_message
	set flag 0
	foreach chan [channels] {
		if {[onchan $text $chan]} {
			putmsg operserv "svspart $text $chan exited"
			set flag 1
		}
	}
	if { $flag == 1 } {
		after 3000 [list newban "$text!*@*" "^CountBlah" "temp by bag" 5m]
	}
    }
}

proc msg:ban {nick host handle text} {
    putlog "ban called from $nick $host $handle $text"
    if [expr [isop $nick] || [ishalfop $nick]] {
	putlog "op confirmed for $nick"
        set bannick [lindex [split $text " "] 0]
        set text_message [join [lrange [split $text " "] 1 end] " "]
#        putmsg $channel $text_message
	if {[string match '*@*' '$bannick']} {
		putmsg $nick "Added ban for $bannick with reason $text_message for eternity"
		newban "$bannick" "$nick" "$text_message" 0
	} else {
		putmsg $nick "Added ban for $bannick!*@* with reason $text_message for eternity"
		newban "$bannick!*@*" "$nick" "$text_message" 0
	}
    }
}

proc msg:banlist {nick host handle text} {
    putlog "banlist called from $nick $host $handle $text"
    if [expr [isop $nick] || [ishalfop $nick]] {
	putlog "op confirmed for $nick"
	putmsg $nick "Ban list:"
	foreach ban [banlist] {
	    set who [lindex $ban 0]
	    set reason [lindex $ban 1]
	    set whobanned [lindex $ban 5]
	    putmsg $nick "Banned: $who for: $reason by: $whobanned"
	}
    }
}

proc msg:gag {nick host handle text} {
    putlog "gag called from $nick $host $handle $text"
    if [expr [isop $nick] || [ishalfop $nick]] {
	putlog "op confirmed for $nick"
        set bannick [lindex [split $text " "] 0]
        set text_message [join [lrange [split $text " "] 1 end] " "]
	putserv "SHUN +$bannick 30m :$text_message"
    }
}

proc join_hello {nick host handle chan} {
  global greetings_data
  global channels_old_nickname
  if { [dict exists $channels_old_nickname $chan] } {
	  set old_nickname [dict get $channels_old_nickname $chan]
  } else {
	  set old_nickname "someone I can't even remember"
  }
#  putserv "PRIVMSG $chan :Hello $nick"
#this is - /me equivalent  proc pub:act {nick uhost handle chan arg} {puthelp "privmsg $chan :\001ACTION $arg\001"}
  if { $nick == "^CountBlah" } {
        return
  }
  if { $nick == "dedicked" } {
	putmsg $chan "It's dedicked's party now, leave your package at the door guys! ( dedicked )"
  	dict set channels_old_nickname "$chan" "$nick"
	return 
  } 
  if { $nick == "dedicked2" } {
	putmsg $chan "It's dedicked's party now, leave your package at the door guys! ( dedicked )"
  	dict set channels_old_nickname "$chan" "$nick"
	return 
  } 
  set length [expr [llength [lindex $greetings_data]] -1]
  set line_number [expr {round(rand()* $length) }]
  set to_put_line [lindex $greetings_data $line_number]
  set first_arg [split $to_put_line " "]
  set text_message [join [lrange [split $to_put_line " "] 1 end] " "]
  set text_message [subst $text_message]
  if { [string match "msg*" "$first_arg"] } {
	#message
	putmsg $chan "$text_message"
  } else {
	# describe
	putact $chan "$text_message"
  }
  dict set channels_old_nickname "$chan" "$nick"
}

proc pubm:meop {nick host handle chan text} {
  global ops_passwords
  global ops_authorised
  putlog "!meop nick=$nick host=$host handle=$handle channel=$chan text=$text"
  if {[info exists ops_passwords($nick)]} {
    if {[info exists ops_authorised($host)]} {
      if { $ops_authorised($host) == "$nick" } {
        putlog "you are now op $chan $nick"
  	putquick "MODE $chan +o $nick"
      }
    }
  }
}

proc msg:rules {nick host handle text} {
  global rules_data
  putlog "rules $nick $host $handle $text"
  set who [lindex $text 0]
  if {$who == ""} {
    set data [split $rules_data "\n"]
    foreach line $data {
        putmsg $nick "$line"
    }
    return 1
  }
  set data [split $rules_data "\n"]
  foreach line $data {
    putmsg $who "$line"
  }
}

proc pubm:rules {nick host handle chan text} {
  global rules_data
  putlog "rules $nick $host $handle $chan $text"
  set who [lindex $text 0]
  if {$who == ""} {
    set data [split $rules_data "\n"]
    foreach line $data {
        putmsg $nick "$line"
    }
    return 1
  }
  set data [split $rules_data "\n"]
  foreach line $data {
    putmsg $who "$line"
  }
}

proc msg:fnick {nick host handle text} {
    putlog "fnick called with $nick $host $handle $text"
    if [expr [isop $nick] || [ishalfop $nick]] {
	set oldnick [lindex $text 0]
	set newnick [lindex $text 1]
	if { $oldnick != "" } {
		if { $newnick != "" } {
			putmsg operserv "svsnick $oldnick $newnick"
		}
	}
    }
}

proc pubm:sides {nick host handle chan text} {
   putlog "sides called from $nick $host $handle $chan $text"
   dumpfile $chan "sides.txt"
}
proc pubm:nav {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:nav unimplemented"
}

proc pubm:noping {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:noping unimplemented"
}

proc pubm:antitalk {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:antitalk unimplemented"
}

proc pubm:at {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:at unimplemented"
}

proc pubm:definition {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:definition unimplemented"
}

proc pubm:trivgame {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:trivgame unimplemented"
}

proc pubm:ps {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:ps unimplemented"
}

proc pubm:birthday {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:birthday unimplemented"
}

proc pubm:sn {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:sn unimplemented"
}

proc pubm:hl {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:hl unimplemented"
}

proc pubm:hp {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:hp unimplemented"
}

proc pubm:fortune {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:fortune unimplemented"
}

proc pubm:bottle {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:bottle unimplemented"
}

proc pubm:modelinks {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:modelinks unimplemented"
}

proc pubm:mboard {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:mboard unimplemented"
}

proc pubm:jop {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:jop unimplemented"
}

proc pubm:jvoice {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:jvoice unimplemented"
}

proc pubm:gbook {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:gbook unimplemented"
}

proc pubm:joinmode {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:joinmode unimplemented"
}

proc pubm:joinset {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:joinset unimplemented"
}

proc pubm:joindefault {nick host handle chan text} {
  #FIXME: implement
  puthelp "NOTICE $nick :pubm:joindefault unimplemented"
}

putlog "loaded greeter"

source /home/unrealircd/eggdrop/scripts/seen.tcl
source /home/unrealircd/eggdrop/scripts/aka.tcl
source /home/unrealircd/eggdrop/scripts/vending.tcl
source /home/unrealircd/eggdrop/scripts/8ball.tcl
source /home/unrealircd/eggdrop/scripts/mc-topiclock.tcl

return 
