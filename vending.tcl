# Bindings
set vending_enabled 1

bind pub - !vendmenu pub_vendmenu
bind msg - !vendmenu msg_vendmenu
bind msg - !vending msg_vending
bind pub - !vending pub_vending

bind pub - !c1  pub_vending_c1 
bind pub - !c2  pub_vending_c2 
bind pub - !c3  pub_vending_c3 
bind pub - !c4  pub_vending_c4 
bind pub - !c5  pub_vending_c5 
bind pub - !c6  pub_vending_c6 
bind pub - !i1  pub_vending_i1 
bind pub - !k1  pub_vending_k1 
bind pub - !k2  pub_vending_k2 
bind pub - !k3  pub_vending_k3 
bind pub - !k4  pub_vending_k4 
bind pub - !d1  pub_vending_d1 
bind pub - !d2  pub_vending_d2 
bind pub - !d3  pub_vending_d3 
bind pub - !d4  pub_vending_d4 
bind pub - !d5  pub_vending_d5 
bind pub - !d6  pub_vending_d6 
bind pub - !d7  pub_vending_d7 
bind pub - !d8  pub_vending_d8 
bind pub - !d9  pub_vending_d9 
bind pub - !e1  pub_vending_e1 
bind pub - !e2  pub_vending_e2 
bind pub - !e3  pub_vending_e3 
bind pub - !e4  pub_vending_e4 
bind pub - !e5  pub_vending_e5 
bind pub - !f1  pub_vending_f1 
bind pub - !f2  pub_vending_f2 
bind pub - !f3  pub_vending_f3 
bind pub - !f4  pub_vending_f4 
bind pub - !f5  pub_vending_f5 
bind pub - !f6  pub_vending_f6 
bind pub - !f7  pub_vending_f7 
bind pub - !f8  pub_vending_f8 
bind pub - !f9  pub_vending_f9 
bind pub - !b12  pub_vending_b12 
bind pub - !s1  pub_vending_s1 
bind pub - !s2  pub_vending_s2 
bind pub - !s3  pub_vending_s3 
bind pub - !s4  pub_vending_s4 
bind pub - !s5  pub_vending_s5 
bind pub - !s6  pub_vending_s6 
bind pub - !s7  pub_vending_s7 
bind pub - !k9  pub_vending_k9 
bind pub - !mar17  pub_vending_mar17 
bind pub - !mar18  pub_vending_mar18 
bind pub - !x1  pub_vending_x1 
bind pub - !z1  pub_vending_z1 
bind pub - !p1  pub_vending_p1 
bind pub - !ds9  pub_vending_ds9 
bind pub - !tng  pub_vending_tng 
bind pub - !voy  pub_vending_voy 
bind pub - !tos  pub_vending_tos 
bind pub - !hhgttg  pub_vending_hhgttg 
bind pub - !jynnantonyx  pub_vending_jynnantonyx 
bind pub - !blessing  pub_vending_blessing 
bind pub - !ptp  pub_vending_ptp 
bind pub - !2b  pub_vending_2b 
bind pub - !g1  pub_vending_g1 
bind pub - !g2  pub_vending_g2 
bind pub - !g3  pub_vending_g3

set fp_chips [open "scripts/data/vending/chips.txt" r]
set chips_data [split [read $fp_chips] "\n"]
close $fp_chips

set fp_chips [open "scripts/data/vending/icecream.txt" r]
set icecream_data [split [read $fp_chips] "\n"]
close $fp_chips

set fp_chips [open "scripts/data/vending/pringles.txt" r]
set pringles_data [split [read $fp_chips] "\n"]
close $fp_chips

proc send_message_file {nick filename} {
  set vending_filename "scripts/data/text/"
  append vending_filename "$filename"
  set vending_file [open "$vending_filename" r]
  set vending_data [split [read $vending_file] "\n"]
  foreach line $vending_data {
	putmsg $nick "$line"
  }
  close $vending_file
}

proc msg_vendmenu {nick uhost hand arg} {
   putlog "vendmenu called from $nick $uhost $hand $arg"
#   dumpfile $nick "vendmenu.txt"
   send_message_file $nick "vendmenu.txt"
}

proc pub_vendmenu {nick uhost hand chan arg} {
   putlog "vendmenu called from $nick $uhost $hand $chan $arg"
#   dumpfile $nick "vendmenu.txt"
   send_message_file $nick "vendmenu.txt"
}

proc vending {where command} {
   global vending_enabled
   if { $command == "on" } {
	set vending_enabled 1
	putmsg $where "The Vending Machine Has Been Enabled. To View The Menu, Type !vendmenu"
   } elseif { $command == "off" } {
	set vending_enabled 0
	putmsg $where "The Vending Machine Has Been Disabled."
   } else {
	set status [expr ($vending_enabled == 1)?"enabled":"disabled"]
	putmsg $where "The Vending Machine is $status"
   }
}

proc msg_vending {nick uhost hand arg} {
   putlog "vending called from $nick $uhost $hand $arg"
   set onoff [lindex [split $arg " "] 0]
   if  {[isop $nick]} {
   	vending $nick $onoff
   }
}

proc pub_vending {nick uhost hand chan arg} {
   putlog "vending called from $nick $uhost $hand $chan $arg"
   set onoff [lindex [split $arg " "] 0]
   if {[isop $nick]} {
   	vending $chan $onoff
   }
}

proc pub_vending_c1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a Reeses"
  }
}

proc pub_vending_c2 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a Starburst"
  }
}

proc pub_vending_c3 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    global chips_data
    set length [llength [lindex $chips_data]]
    set line_number [expr {round(rand()* $length) }]
    set to_put_line [lindex $chips_data $line_number]
    putmsg $chan "$nick gets a bag of $to_put_line flavoured potato chips"
  }
}

proc pub_vending_c4 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a Snickers bar"
  }
}

proc pub_vending_c5 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a Hershey's Bar"
  }
}

proc pub_vending_c6 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "Passes $nick a Spree" 
  }
}

proc pub_vending_i1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    global icecream_data
    set length [llength [lindex $icecream_data]]
    set line_number [expr {round(rand()* $length) }]
    set to_put_line [lindex $icecream_data $line_number]
    putmsg $chan "$nick gets a cone with a nice big scoop of $to_put_line ice cream"
  }
}

proc pub_vending_k1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a King Size Reeses"
  }
}

proc pub_vending_k2 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a King Size Starburst"
  }
}

proc pub_vending_k3 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a King Size Snickers Bar"
  }
}

proc pub_vending_k4 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a King Size Twix"
  }
}

proc pub_vending_d1 {nick uhost hand chan arg} {

  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a can of Dr. Pepper"
  }
}

proc pub_vending_d2 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a can of Pepsi"
  }
}

proc pub_vending_d3 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a can of Orange Soda"
  }
}

proc pub_vending_d4 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a can of Coca Cola"
  }
}

proc pub_vending_d5 {nick uhost hand chan arg} {

  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a nice hot, fresh cup of coffee"
  }
}

proc pub_vending_d6 {nick uhost hand chan arg} {

  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a can of Cream Soda"
  }
}

proc pub_vending_d7 {nick uhost hand chan arg} {

  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a can of Canada Dry Ginger Ale"
  }
}

proc pub_vending_d8 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets an icy mug of A&W root Beer"
  }
}

proc pub_vending_d9 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a cold, refreshing Sundrop soda"
  }
}

proc pub_vending_e1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a Three Musketeers bar"
  }
}

proc pub_vending_e2 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a Moon Pie"
  }
}

proc pub_vending_e3 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets an Almond Joy bar"
  }
}

proc pub_vending_e4 {nick uhost hand chan arg} {

  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a Butterfinger"
  }
}

proc pub_vending_e5 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a Pay Day bar"
  }
}

proc pub_vending_f1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a beautiful fresh bacon, lettuce and tomato sandwich"
  }
}

proc pub_vending_f2 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a nice warm grilled chese sandwich"
  }
}

proc pub_vending_f3 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a classic bologna sandwich"
  }
}

proc pub_vending_f4 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a bratwurst on a bun, loaded with sauerkraut"
  }
}

proc pub_vending_f5 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a bun with a kielbasa and smothered in peppers and onions"
  }
}

proc pub_vending_f6 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets the best Philly Cheesesteak to be found outside Philadelphia"
  }
}

proc pub_vending_f7 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a wonderful lamb gyro"
  }
}

proc pub_vending_f8 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a nice plate of  spicy haggis"
  }
}

proc pub_vending_f9 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a nice bowl of poutine, so fresh the curds squeak"
  }
}

proc pub_vending_b12 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick sank my battleship!"
  }
}

proc pub_vending_s1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a piece of delicioius, fresh key lime pie"
  }
}

proc pub_vending_s2 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {

    putmsg $chan "$nick gets a nice big piece of black forest cake"
  }
}

proc pub_vending_s3 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a thick slice of sponge cake"
  }
}

proc pub_vending_s4 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a slice of New York-style cheesecake"
  }
}

proc pub_vending_s5 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a slice of Wetherman's award-winning pecan pie"
  }
}

proc pub_vending_s6 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a slice of fresh strawberry-rhubarb custard pie"
  }
}

proc pub_vending_s7 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a slice of angelfood cake"
  }
}

proc pub_vending_k9 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a milkbone"
  }
}

proc pub_vending_mar17 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a pint of Guinness, a plate of corned beef with cabbage, some soda bread, and some colcannon. "
  }
}

proc pub_vending_mar18 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putact $chan "kicks $nick in the head, throws them to the ground, holds a gun to their temple, and tells them there is no such day!"
  }
}

proc pub_vending_x1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a roll of Tums, some Bromo-Seltzer, and some Pepto"
  }
}

proc pub_vending_z1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a box of crayons and a brand-new colouring book"
  }
}

proc pub_vending_p1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    global pringles_data
    set length [llength [lindex $pringles_data]]
    set line_number [expr {round(rand()* $length) }]
    set to_put_line [lindex $pringles_data $line_number]
    putmsg $chan "$nick gets a can of $to_put_line Pringles"
  }
}

proc pub_vending_ds9 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a nice cup of raktajino"
  }
}

proc pub_vending_tng {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a nice cup of Earl Grey tea"
  }
}

proc pub_vending_voy {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a nice cup of black coffee"
  }
}

proc pub_vending_tos {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a potent cup of Romulan Ale"
  }
}

proc pub_vending_hhgttg {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a foaming, bubbling Pan-Galactic Gargle Blaster, as well as a list of recovery centres" 
  }
}

proc pub_vending_jynnantonyx {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a nice glass of tonic water, with several jiggers of Bombay Sapphire, ice, and a slice of lime"
  }
}

proc pub_vending_blessing {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putmsg $chan "$nick gets a nice, freshly-brewed, custom-blend coffee, with a generous portion of Irish whisky, sugar, and topped with thick cream"
  }
}

proc pub_vending_ptp {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putact $chan "passes $nick a tub of freshly-popped popcorn, topped with melted butter and a sprinkling of salt."
  }
}

proc pub_vending_2b {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
   putact $chan "walks to the center of the stage, and starts soliloquizing... "
   putmsg $chan "To be, or not to be, that is the question... umm... wait, where was I? Nevermind then."
  }
}

proc pub_vending_g1 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putact $chan "places before $nick a plate containing some freshly-made crumpets, along with several small containers of clotted cream, marmalade, lemon curd, and assorted preserves for their perusal."
  }
}

proc pub_vending_g2 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putact $chan "serves $nick a plate with some lightly toasted tea biscuits, generously loaded with sweet butter."
  }
}

proc pub_vending_g3 {nick uhost hand chan arg} {
  global vending_enabled
  if { $vending_enabled == 1 } {
    putact $chan "sets a plate down before $nick, loaded with a bacon double cheeseburger, fries, and onion rings."
  }
}
