#################################################
#                                               #
#               Topic Lock Script               #
#                  For Eggdrop                  #
#                     v1.00                     #
#                                               #
#           Eggdrop v1.6.11 or later!           #
#                                               #
#                  Scripted by                  #
#               MikeC on QuakeNet               #
#                                               #
#################################################
# After using a number of topic lock scripts by #
# various authors, I decided to write my own.   #
#                                               #
# I didn't want to have to use special commands #
# just to lock a topic.                         #
#                                               #
# This script works using the +T custom flag.   #
#                                               #
# The topic is 'locked' when a privilaged user  #
# sets the topic in the normal way.  Privilaged #
# users are owners/masters/+T, as shown below:  #
#                                               #
#     Global +n/+m/+T      Channel +n/+m/+T     #
#                                               #
# If a user changes the topic, and they are not #
# privilaged, the bot will revert the topic to  #
# the previously saved one.                     #
#                                               #
# Topics are also saved to disk, so are saved   #
# when the bot resets. (Thanks to Paladinz)     #
#                                               #
# Only channels set +topiclock will be affected #
# To enable, do ".chanset #channel +topiclock"  #
#                                               #
# Please note that when running the script for  #
# the first time, any user will be able to set  #
# the topic, until a privilaged user sets it.   #
#                                               #
# To sum up, once the topic has been set by an  #
# owner/master/+T user, only privilaged users   #
# can change the topic.  Normal users changing  #
# it will find the topic is changed back.       #
#################################################


##################################################
## Folder for stored topics (No trailing slash) ##
##################################################

set TL_Folder "/home/unrealircd/eggdrop/topics"




####################################################################################################
#  DO NOT EDIT BELOW THIS LINE!  #  DO NOT EDIT BELOW THIS LINE!  #  DO NOT EDIT BELOW THIS LINE!  #
####################################################################################################




#################################
## Bind topic change to a proc ##
#################################

bind topc - * TL_TopicChange


#####################################
## Define per-channel flag setting ##
#####################################

setudef flag topiclock


#######################################
## Do this when the topic is changed ##
#######################################

proc TL_TopicChange {nick uhost hand chan topic} {
  global botnick TL_Locked TL_Topic
  if {[channel get $chan topiclock]} {
    if {([string tolower $nick] != [string tolower $botnick])} {
      if {([matchattr $hand n]) || ([matchattr $hand |n $chan]) || ([matchattr $hand m]) || ([matchattr $hand |m $chan]) || ([matchattr $hand T]) || ([matchattr $hand |T $chan])} {
        TL_TopicLock $chan "$topic"
      } else {
        if {([info exists TL_Locked([string tolower $chan])]) && (([info exists TL_Topic([string tolower $chan])]) && ($TL_Locked([string tolower $chan]))) && ($topic != $TL_Topic([string tolower $chan]))} {
          if {([botisop $chan]) || (![string match "*t*" [lindex [getchanmode $chan] 0]])} {
            putserv "TOPIC $chan :$TL_Topic([string tolower $chan])"
          }
        }
      }
    }
  }
}


#########################################
## Lock topic for channel & write file ##
#########################################

proc TL_TopicLock {chan topic} {
  global TL_Folder TL_Locked TL_Topic
  set TL_File $TL_Folder/[string tolower $chan].topic
  set TL_Locked([string tolower $chan]) 1
  set TL_Topic([string tolower $chan]) $topic
  set File [open $TL_File w]
  puts $File "$topic"
  close $File
}




####################################################################################################
####################################################################################################




######################################
## Show script is loaded on startup ##
######################################

putlog "\002TopicLock\002 script loaded"


##############################################
## Read stored topics from files on startup ##
##############################################

foreach chan [channels] {
  global TL_Folder TL_Locked TL_Topic
  set TL_File $TL_Folder/[string tolower $chan].topic
  if [file exists $TL_File] {
    if {[channel get $chan topiclock]} {
      set File [open $TL_File r]
      gets $File line
      if {![string match "none*" $line]} {
        set TL_Locked([string tolower $chan]) 1
        set TL_Topic([string tolower $chan]) $line
        putlog "\002TopicLock\002 Holding topic of $chan ($TL_Topic([string tolower $chan]))"
      } else {
        set TL_Locked([string tolower $chan]) 0
      }
      close $File
    } else {
      file delete $TL_File
    }
  } else {
    set TL_Locked([string tolower $chan]) 0
  }
}


###################
## END OF SCRIPT ##
###################
