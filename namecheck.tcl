proc stringDistance {a b} {
    set n [string length $a]
    set m [string length $b]
    for {set i 0} {$i<=$n} {incr i} {set c($i,0) $i}
    for {set j 0} {$j<=$m} {incr j} {set c(0,$j) $j}
    for {set i 1} {$i<=$n} {incr i} {
        for {set j 1} {$j<=$m} {incr j} {
            set x [expr {$c([- $i 1],$j)+1}]
            set y [expr {$c($i,[- $j 1])+1}]
            set z $c([- $i 1],[- $j 1])
            if {[string index $a [- $i 1]]!=[string index $b [- $j 1]]} {
                incr z
            }
            set c($i,$j) [min $x $y $z]
        }
    }
    set c($n,$m)
}

# some little helpers:
if {[catch {
    # DKF - these things (or rather improved versions) are provided by the 8.5 core
    package require Tcl 8.5
    namespace path {tcl::mathfunc tcl::mathop}
}]} then {
    proc min args {lindex [lsort -real $args] 0}
    proc max args {lindex [lsort -real $args] end}
    proc - {p q} {expr {$p-$q}}
}

proc groupdo { lst } {
	set i 0
	foreach a $lst {
		set j 0
		set str ""
		foreach b $lst {
			set tmp [stringDistance $a $b]
			set c($i,$j) $tmp
			set str "$str [format "%3s" $tmp]"
			incr j
		}
		incr i
	}
	return [array get c]
}

proc limit { arr size thresh } {
	foreach { key value } $arr {
		if { $value <= $thresh } {
			set arr1($key) $value
		} else {
			set arr1($key) 0
		}
	}
	return [array get arr1]
}

proc dfs { arr v comp used size } {
	upvar $comp cmp $used usedl
	array set larr "$arr"
	set usedl($v) 1
	lappend cmp $v
	for { set i 0 } { $i < $size } { incr i } {
		if { $larr($i,$v) > 0 } {
			set to $i
			if { $usedl($to) == 0 } {
				dfs $arr $to cmp usedl $size
			}
		}
	}
}

proc find_comps { arr size } {
	set output [list]
	set comp [list]
	for {set i 0} {$i < $size } {incr i} {
		set used($i) 0
	}
	for {set i 0} {$i < $size } {incr i} {
		if { $used($i) == 0 } {
			set comp [list]
			dfs $arr $i comp used $size
			lappend output $comp
		}
	}
	return $output
}

#set mainlist [list]
#
#set lst [list lightIRC_7167 lightIRC_820 test user lightIRC_9834 Scaramouche fhunter_ fhunter]
#lappend mainlist $lst
#set lst [list interestedampa real rbkamp interestedamp2 eunuchtom interestedampII interestedamp_ interestedamp]
#lappend mainlist $lst
#set lst [list sklavenhoden cbt-sklavensau cbt-sklave langersack langer-sack eiersack-devot dev-kloetensack sklavensack]
#lappend mainlist $lst
#set lst [list HoofinMouth Cthuluhu MacCthuluhu MacTheRussian MacTheWolf MacDuck MacWolf]
#lappend mainlist $lst
#
#foreach lst $mainlist {
#	set len [llength $lst]
#	puts "length = $len list=$lst"
#	set group [groupdo $lst]
#	set limited [limit $group $len 1]
#	set groups [find_comps $limited $len]
#	puts "grouplen=[llength $groups]"
#	#puts "$group | $limited"
#	foreach k $groups {
#		set str1 ""
#		foreach j $k {
#			set str1 "$str1 [lindex $lst $j]"
#		}
#		puts "Grouping=$str1"
#	}
#}
#
